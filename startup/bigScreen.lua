local version = .0061
shell.run(util)
local util = require("/util")

local resourceDir = "resources/movies/"
local screenList = { peripheral.find("monitor") }

 local screenMap = {
    [1] = 8,
    [2] = 12,
    [3] = 10,
    [4] = 7,
    [5] = 11,
    [6] = 5,
    [7] = 6,
    [8] = 2,
    [9] = 1,
    [10] = 9,
    [11] = 4,
    [12] = 3
 }

local function playFrame(frameName)
    for i,mon in pairs(screenList) do
        mon.clear()
        mon.setTextScale(0.5)
        mon.setCursorPos(1,1)
        --mon.write("Monitor #"..i)
        --mon.setCursorPos(1,2)
        --mon.write("Position #"..screenMap[i])
        --term.redirect(mon)
        
        local map = screenMap[i]-1
        --print("i = "..i)
        --print("map = "..map)
        
        local frameFile = frameName..map

        if fs.exists(resourceDir..frameFile..".lua") then
            local image = require("/"..resourceDir..frameFile)
            image.draw(mon)
            --paintutils.drawImage(image, 0, 0)
            --local image = paintutils.loadImage(resourceDir..frameFile)
        else
            print("Can't find frame: "..resourceDir..frameFile..".lua")
            --do something here to download them
            util.downloadResources(resourceDir,frameFile..".lua")
            sleep(1)
        end

    end
end

--local old_term = term.current()
--local tv = peripheral.find("monitor") --find and attach to monitor
--tv.setTextScale(0.5)
--tv.clear()

local old_term = term.current()

while true do
    playFrame("terraria-")
    sleep(1)
end

term.redirect(old_term)


--util.playMovie(movieList)
