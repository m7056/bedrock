local version = .0008
shell.run(util)
local util = require("/util")

local adList = {
    "bobaFatAd.nfp",
    "blazerod_ad.nfp",
    "soup_ad.nfp",
    "bachelorchow_ad.nfp",
    "pabstBlueRobot_ad.nfp",
    "slurm_ad.nfp",
    "walrusjuice_ad.nfp",
    "levelYourTool.nfp"
}

util.downloadResources("resources/billboards/", adList)

local old_term = term.current()
local tv = peripheral.find("monitor") --find and attach to monitor
tv.setTextScale(0.5)
term.redirect(tv)

util.adLoop(adList,6)
