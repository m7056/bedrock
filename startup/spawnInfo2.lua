print("spawnInfo2.lua exists")

local util = require("util")

util.update("rightMonitor2.txt")
util.update("leftMonitor2.txt")

leftMonitor = peripheral.wrap("left")
rightMonitor = peripheral.wrap("right")

rightMonitor.setCursorPos(1,1) --Sets the cursor pos. This is the start of the text.
rightMonitor.setTextScale(1) --Increase the text scale to 2.

function txt2Screen(txtFile,screen)
    data = io.open(txtFile)
    local lineNumber = 1
    for line in data:lines() do
        if screen == "right" then
            if lineNumber == 1 then rightMonitor.clear() end
            rightMonitor.setCursorPos(1,lineNumber)
            rightMonitor.write(line)
            lineNumber = lineNumber + 1

        elseif screen == "left" then
            if lineNumber == 1 then leftMonitor.clear() end
            leftMonitor.setCursorPos(1,lineNumber)
            leftMonitor.write(line)
            lineNumber = lineNumber + 1

        else
            print("screen needs to be left or right")
        end

    end

    data:close()
end

txt2Screen("rightMonitor.txt", "right")
txt2Screen("leftMonitor.txt", "left")
