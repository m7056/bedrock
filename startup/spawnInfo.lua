local version = .0920
shell.run(util)
local util = require("/util")

local textList = {
    "serverInfo.txt",
    "serverHowto1.txt"
}

local myScreens = { peripheral.find("monitor") }

util.downloadResources("resources/screentext/", textList)

while true do
    util.txt2Screen(textList[1], myScreens[1])
    util.txt2Screen(textList[2], myScreens[2])
    sleep(20)
end

