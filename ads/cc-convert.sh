#!/usr/bin/bash

MOVIEFOLDER="/home/mmance/dev/bedrock/resources/movies"

JUROKU="/home/mmance/go/bin/juroku"
IFILE=$1
NEWFILE=`echo $1 | sed s/.png//g`

convert $IFILE -crop 4x3@ $NEWFILE-%d.png

for f in $NEWFILE-*.png; do
	NEWNAME=`echo $f | sed s/.png//g`
	echo "converting $NEWNAME.lua to $f"
	$JUROKU -o $NEWNAME.lua $f
done

#rm $NEWFILE*.png
rm preview.png

mv *.lua $MOVIEFOLDER

