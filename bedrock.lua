function checkFuel()
    if turtle.getFuelLevel() < 1000 then
        for i = 15,1,-1 do
            turtle.select(i)
            turtle.refuel()
        end
    end
end

function dumpCrap()
    for i = 16, 1, -1 do
        if turtle.getItemCount(i) > 0 then
            local data = turtle.getItemDetail(i)
            if data then
                if shouldIDig(data.name) == false then
                    turtle.select(i)
                    turtle.drop()
                end
            end
        end
    end
end

function dropLoot()
    if turtle.getItemCount(16) > 0 then
        if turtle.getItemDetail(16).name == "minecraft:chest" then

            local exists, data = turtle.inspectUp()
            if exists then turtle.digUp() end
                turtle.select(16)
                turtle.placeUp()

                for i = 15,1,-1 do
                    turtle.select(i)
                    turtle.dropUp()
                end
        end
    end
end

function spinDig()
    if levelCount < -1 then
        for i=4,1,-1 do
          if turtle.getItemCount(15) > 0 then
            dumpCrap()
          end
          smartDig()
          turtle.turnLeft()
        end
    end
end

function locateBlock(blockName)
    local blockSlot = nil
    for i=15,1,-1 do
        if turtle.getItemDetail(i) then
            blockSlot = true
            turtle.select(i)
            break
        end
    end
    return blockSlot
end

function plugHole(direction)
    if locateBlock("minecraft:dirt") then
        print("using dirt")
    elseif locateBlock("minecraft:cobblestone") then
        print("using cobble")
    else
        turtle.select(1)
    end

    if direction == "up" then
        turtle.placeUp()
    end
    if direction == "down" then
        turtle.placeDown()
    end
end


function shouldIDig(blockToCheck)

    --can lead to following veins of unknown blocks
    local digBlock = true

    for _,valueBlock in ipairs(valueBlocks) do
          if blockToCheck == valueBlock then
            digBlock = true
          end
    end

    for _,junkBlock in ipairs(junkBlocks) do
      if blockToCheck == junkBlock then
        digBlock = false
      end
    end

    return digBlock

end

function digDeeper()

    repeat turtle.dig()
    until turtle.forward()
    spinDig()
    repeat until turtle.back()

end

function smartDig()

  local blockExists, data = turtle.inspect()
  local blockExistsDown, dataDown = turtle.inspectDown()
  local blockExistsUp, dataUp = turtle.inspectUp()

  local goDeeper = false

  if blockExists then
    if shouldIDig(data.name) then
      if turtle.dig() then
        digDeepCount = digDeepCount + 1
        if digDeepCount <= maxDigDeeper then
            goDeeper = true
            print("depth: "..digDeepCount)
        else
            digDeepCount = 0
        end
        print("Mining: "..data.name)
      end
    else
        digDeepCount = 0
    end
    if goDeeper == true then
        digDeeper()
    end
 end

  if blockExistsUp then
    if shouldIDig(dataUp.name) then
        if turtle.digUp() then
            print("Mining: "..dataUp.name)
        end
    end
  end

  if blockExistsDown then
      if shouldIDig(dataDown.name) then
          if turtle.digDown() then
              print("Mining: "..dataDown.name)
          end
      end
  end

end

function decend()
  local hitBedrock = false

  if levelCount == -1 then
    plugHole("up")
  end

  while hitBedrock == false do

    blockExists,data = turtle.inspectDown()

    if data.name == "minecraft:bedrock" then
        hitBedrock = true
        break
    end

    if blockExists then
       turtle.digDown()
    end

    if turtle.down() then
        levelCount = levelCount - 1
    end

    spinDig()

  end

end

function accend()
	print("accend")
    repeat
        if turtle.up() then
            levelCount = levelCount + 1
        else
            turtle.digUp()
        end
        spinDig()
    until levelCount == 0
end

function safeHeight()
    print("safeHeight")
    local safeHeight = 5
    local currentLevel = levelCount

    repeat
        if turtle.up() then
            levelCount = levelCount + 1
            print("level: "..levelCount)
        end
    until levelCount == currentLevel + safeHeight
end

function nextHole()
	print("nextHole")
    local moves = 3

    while moves > 0 do
        if turtle.forward() then
            moves = moves - 1
        else
          turtle.dig()
        end
    end
end

function init()
    junkBlocks = {
       "minecraft:cobblestone",
       "minecraft:stone",
       "minecraft:grass",
       "minecraft:gravel",
       "quark:limestone",
       "minecraft:dirt",
       "minecraft:lava",
       "minecraft:sand",
       "minecraft:red_sandstone",
       "minecraft:flowing_lava",
       "minecraft:water",
       "minecraft:leaves",
       "minecraft:leaves2",
       "minecraft:flowing_water",
       "minecraft:netherrack",
       "minecraft:soul_sand",
       "minecraft:bedrock",
       "projectred-exploration:stone",
       "chisel:limestone",
       "chisel:limestone2",
       "chisel:marble",
       "chisel:marble2",
       "appliedenergistics2:sky_stone_block",
       "quark:biome_cobblestone"
    }

    buildBlocks = {
        "minecraft:stone",
        "minecraft:cobblestone"
    }

    valueBlocks = {
        "minecraft:iron_ore",
        "minecraft:gold_ore",
        "minecraft:coal_ore",
        "minecraft:lapis_ore",
        "minecraft:redstone_ore",
        "minecraft:diamond_ore",
        "minecraft:emerald_ore",
        "minecraft:netherquartz_ore"
    }

    chestBlocks = {
        "minecraft:chest",
        "quark:custom_chest"
    }

    levelCount = 0
    digDeepCount = 0
    maxDigDeeper = 16

end

init()

checkFuel()
decend()
safeHeight()
nextHole()
decend()
dumpCrap()
safeHeight()
checkFuel()
accend()
plugHole("down")
dumpCrap()
dropLoot()
nextHole()
