-- INSTALL INFORMATION --
-- wget https://gitlab.com/m7056/bedrock/-/raw/main/util.lua
-- pastebin get http://pastebin.com/6DLa84hg bedit.lua

-- SET GLOBALS
local version = 2.20637
local serverURL = "https://gitlab.com/m7056/bedrock/-/raw/main/"

local function getVersion(scriptName)
    local versionFound = 0

    if not fs.exists(scriptName) then
        print("ERROR: Invalid scriptName, file not found: "..scriptName)
    else
        local handle = fs.open(scriptName, "r")
        while true do
            local line = handle.readLine()
            if line then
                local a,b = string.find(line, "local.version.=.")
                if a and b then
                    --found the version line
                    versionFound = string.sub(line, b+1)
                    return versionFound
                end
            else
                break
            end
        end
        handle.close()
        if versionFound == 0 then
            print("ERROR: Found File: "..scriptName..", but could not locate version")
        end
    end

    return versionFound
end

local function update(...)
    for i,v in ipairs(arg) do

        local updateFile = false
        local scriptName = tostring(v)
        local lockFile = "/tmp/"..scriptName..".lock"
        local getFile = serverURL..scriptName
        local localVersion = 0
        local serverVersion = 0

        print("INFO: Checking for update: "..scriptName)

        --try to download file from server
        shell.run("wget", getFile, lockFile)

        --no need to check version if I don't already have it
        if fs.exists("/"..scriptName) then
            localVersion = getVersion(scriptName)
            if fs.exists(lockFile) then
                serverVersion = getVersion(lockFile)
                if serverVersion > localVersion then
                    updateFile = true
                    --update found
                else
                    --update not found
                end
            else
                print("ERROR: File "..getFile.." NOT FOUND")
            end
        else
            --just update without version check, we dont have that file yet.
            print("INFO: No install found, Installing")
            updateFile = true
        end

        if updateFile then
            print("INFO: Found Update...  Installing v"..serverVersion)
            fs.delete(scriptName)
            if fs.exists(scriptName) then
                print("ERROR: Delete Failed")
            else
                print("lockfile:"..lockFile)
                fs.move(lockFile, scriptName)
                if fs.exists(scriptName) then
                    print("INFO: Install Successful")
                    return true
                else
                    print("ERROR: Install Failed")
                    return false
                end
            end

        else
            fs.delete(lockFile)
            print("INFO: local version:"..localVersion.." server version:"..serverVersion)
            return false
        end
    end
end


local function downloadResources(folder, fileList)

    if type(fileList) == "string" then

        local serverFile = serverURL..folder..fileList
        local localFile = folder..fileList

        print("serverFile: "..serverFile)
        print("localFile: ".. localFile)

        shell.run("wget", serverFile, localFile)
    
        if not fs.exists(localFile) then
            print("I d/l but I can't find "..localFile)
        end
    
    else
        print("filelist is table")
        for _,file in pairs(fileList) do

            if not file then break end

            local serverFile = serverURL..folder..file
            local localFile = folder..file

            if fs.exists(localFile) then
                fs.delete(localFile)
            else
                print("Downloading: "..serverFile)
                shell.run("wget", serverFile, localFile)
                sleep(1)
            end
        end     
    end
end

local function installMBS()
    --D/L Mildly Better Shell if missing.
    if not fs.isDir("/.mbs") then
        shell.run("wget", "https://raw.githubusercontent.com/SquidDev-CC/mbs/master/mbs.lua")
        shell.run("mbs", "install")
        shell.run("reboot")
    end
end

local function updateStartup()
    local computerName = os.getComputerLabel()
    if update("startup/"..computerName..".lua") then
        shell.run("reboot")
    end
end

local function updateUtil()
    if update("util.lua") then
        shell.run("reboot")
    end
end

local function txt2Screen(txtFile,screen)
    local filePath = "resources/screentext/"..txtFile
    if fs.exists(filePath) then
        local data = io.open(filePath)
        print("file: "..filePath)
        screen.clear()
        local lineNumber = 1
        for line in data:lines() do
            screen.setCursorPos(1,lineNumber)
            screen.write(line)
            lineNumber = lineNumber + 1
        end
        data:close()
    else
        print("Could not find: "..filePath)
    end
end

local function adLoop(fileList, pauseLen)

    local adFolder = "resources/billboards/"

    while true do
        for _,ad in pairs(fileList) do
            term.clear()
            if ad then
                if fs.exists(adFolder..ad) then
                    local image = paintutils.loadImage(adFolder..ad)
                    paintutils.drawImage(image, 0, 0)
                else
                    print("Can't find ad: "..adFolder..ad)
                    --do something here to download them
                    downloadResources(adFolder,ad)
                    
                end
                sleep(pauseLen)
            else
                break
            end
        end
    end
end

local function playMovie(fileList)
    for _,file in ipairs(fileList) do
        print(file)
    end
end


installMBS()
updateUtil()
updateStartup()

return {
downloadResources = downloadResources,
update = update,
getVersion = getVersion,
txt2Screen = txt2Screen,
adLoop = adLoop,
playMovie = playMovie
}
