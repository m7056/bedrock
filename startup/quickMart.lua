print("spawnInfo.lua exists")

local util = require("util")

util.update("quickMart.txt")

topMonitor = peripheral.find("monitor")

topMonitor.setCursorPos(1,1) --Sets the cursor pos. This is the start of the text.
topMonitor.setTextScale(2) --Increase the text scale to 2.

function txt2Screen(txtFile)
    data = io.open(txtFile)
    local lineNumber = 1
    for line in data:lines() do
        if lineNumber == 1 then topMonitor.clear() end
        topMonitor.setCursorPos(1,lineNumber)
        topMonitor.write(line)
        lineNumber = lineNumber + 1
    end
    data:close()
end

txt2Screen("quickMart.txt")
